const { expect } = require("chai")
const currencyUseCase = require("../../../src/domain/currency/usecase")()

describe("currency domain", () => {
    it("Default currency should be euro symbol", () => {
        return currencyUseCase.getDefaultCurrency()
            .then(defaultCurrency => expect(defaultCurrency).to.be.equal('€'));
    });
});
